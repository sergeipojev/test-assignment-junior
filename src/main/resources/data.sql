INSERT INTO books VALUES ('0195153448','Classical Mythology','Mark P. O. Morford',2002,'Oxford University Press');
INSERT INTO books VALUES ('0002005018','Clara Callan','Richard Bruce Wright',2001,'HarperFlamingo Canada');

INSERT INTO book_ratings VALUES (rating_seq.nextval, '0195153448',0);
INSERT INTO book_ratings VALUES (rating_seq.nextval, '0002005018',10);